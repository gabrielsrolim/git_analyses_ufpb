# -*- coding: utf-8 -*-

from bd.connect import connect,syncdb
from gitanalyse.teste_cad import cad_desenvolvedor
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--syncdb",action = 'store_true',help="Cria todas as tabelas que ainda faltam no banco de dados.")
    args = parser.parse_args()
    
    if args.syncdb:
        engine,session = connect('default')
        syncdb(engine)
    else:
        print 'Do nothing!'
    print  session
    cad_desenvolvedor(engine,session)
    


