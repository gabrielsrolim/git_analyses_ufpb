# sqlalchemytest.py

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///:memory:',echo=True)

engine.execute("select 1").scalar()

Base = declarative_base()

class User(Base):
	__tablename__ = 'users'

	id = Column(Integer, primary_key=True)
	name = Column(String(50))
	fullname = Column(String(50))
	password = Column(String(12))

	def __init__(self,name,fullname, password):
		self.name = name
		self.fullname = fullname
		self.password = password

	def __repr__(self):
		return "<User('%s','%s','%s')>" % (self.name, self.fullname, self.password)

Base.metadata.create_all(engine)

ed_user = User('ed','Ed Jone','edspassword')
ed_user.name
ed_user.password
str(ed_user.id)

Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

ed_user = User('ed','Ed Jones','edspassword')
session.add(ed_user)

our_user = session.query(User).filter_by(name='ed').first()
our_user
ed_user is our_user

session.add_all([
	User('wendy', 'Wendy Williams', 'foobar'),
	User('mary', 'Mary Contrary', 'xxg527'),
	User('fred', 'Fred Flinstone', 'blah')])

ed_user.password = 'f8s7ccs'
session.dirty
session.new
session.commit()

ed_user.id 

ed_user.name = 'Edwardo'
fake_user = User('fakeuser', 'Invalid', '12345')
session.add(fake_user)

session.query(User).filter(User.name.in_(['Edwardo', 'fakeuser'])).all()

session.rollback()
ed_user.name 

fake_user in session

session.query(User).filter(User.name.in_(['ed', 'fakeuser'])).all() 

#Querying

for instance in session.query(User).order_by(User.id):
	print instance.name, instance.fullname

for name, fullname in session.query(User.name, User.fullname): 
	print name, fullname

for row in session.query(User, User.name).all(): 
	print row.User, row.name


for row in session.query(User.name.label('name_label')).all():
	print(row.name_label)


obj = engine.execute('select p.name as name_project,s.version as version,m.name as name_metric,m.id as id_metric,pm.value as metric_value,p.id as id_project from project_measures pm join snapshots s on (s.id = pm.snapshot_id) join metrics m on (m.id = pm.metric_id) join projects p on (p.id = s.project_id) where pm.value is not null and pm.value <> 0 and m.id in (1,2,3,4,5,6,7,8,9,20,22,114,115,116,118,122,123,124)')

fetch = obj.fetchall()
fetch[0]['name_project']
