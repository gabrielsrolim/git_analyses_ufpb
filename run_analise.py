# -*- coding: utf-8 -*-
import argparse
from gitanalyse.analisar import analisarRepositorio


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--invert",action = 'store_true',help="Inverte a ordem dos commits")
    parser.add_argument("-n","--nameproject",help="Nome do Projeto")
    parser.add_argument("-p","--path",help="Caminho do repositorio a ser analisado")
    parser.add_argument("-ca","--comandant",help="Comandos adicionais antes de rodar o ant.")
    parser.add_argument("-car","--comandaddant",help="Comandos adicionais ao rodar o ant.")
    parser.add_argument("-s","--source",help="Informa o source do projeto quando for rodar o sonar. default = 'java'")
    parser.add_argument("-b","--binaries",help="Informa onde está as classes para ser analisado pelo sonar. default = 'output/classes'")
    args = parser.parse_args()
    configAnalises = dict()

    if args.invert:
        configAnalises['commit_reverse'] = True

    if args.nameproject:
        configAnalises['projectname'] = args.nameproject
    else:
        configAnalises['projectname'] = 'tomcat'

    if args.comandant:
        configAnalises['comandant'] = args.comandant

    if args.source:
        configAnalises['source'] = args.source
    else:
        configAnalises['source'] = 'java'

    if args.comandaddant:
        configAnalises['comandaddant'] = args.comandaddant



    if args.binaries:
        configAnalises['binaries'] = args.binaries
    else:
        configAnalises['binaries'] = 'output/classes'

    #print configAnalises['commit_reverse']
    print configAnalises['projectname']
    #print configAnalises['comandant']
    print configAnalises['source']
    print configAnalises['binaries']



    if args.path:
        analisarRepositorio(args.path,configAnalises)
    else:
        print 'Informe o caminho do projeto.'
