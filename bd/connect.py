from sqlalchemy import create_engine
from gitanalyse.settings import *
from bd.models import *
from sqlalchemy.orm import sessionmaker

def connect(database,verbose = False):
    if Teste:
        database = DATABASES['teste']
    else:
        database = DATABASES[database]

    if len(database['ENGINE']) > 0:
        urlEngine = database['ENGINE']+'://'
    
    if len(database['USER'])>0 and database['PASSWORD'] > 0:
        if len(database['HOST'])>0 and database['PORT'] >0:
            urlEngine = urlEngine +database['USER']+':'+database['PASSWORD']+'@'
            urlEngine = urlEngine +database['HOST']+':'+str(database['PORT'])

    if len(database['NAME']) > 0:
        urlEngine = urlEngine +'/'+database['NAME']

    engine = create_engine(urlEngine,echo=verbose)
    Session = sessionmaker(bind=engine)
    Session.configure(bind=engine) 
    session = Session()
    return engine,session

def syncdb(engine):
	Base.metadata.create_all(engine)

