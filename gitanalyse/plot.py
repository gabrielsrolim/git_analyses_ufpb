import matplotlib.pyplot as plt
import time

class PlotAnalises():

    def __init__(self,engine,nameProject='tomcat',plothora = True,plotcommit = False,delta=False,showBugs=False):
        self.engine = engine;
        self.nameProject = nameProject
        self.plothora = plothora
        self.plotcommit = plotcommit
        self.delta = delta
        self.showBugs = showBugs

    
    def plotGraph(self,fetch,title,xlabel = u'Versao',ylabel = u'Valor',nomeImg=u'default.png'):
        i = 0
        x = []
        y = []
        text = []
        y_delta = []
        
        print '\n'+title
        for row in fetch:
            print 'version: %s Value:%.2f' %( row['version'],round(float(row['value']),2))
            #print 'version: %.2f Value:%.2f' %( round(float(row['version']),2),round(float(row['value']),2))
            #plt.plot(float(row['version']),float(row['value']))
            #x.append(round(float(row['version']),2))
            #y.append(round(float(row['value']),2))
            if self.plothora and self.plotcommit:
                text.append(row['version'])
            elif self.plothora:
                version = row['version']
                list_ver = version.split('#')
                text.append(list_ver[0])
            else:
                version = row['version']
                list_ver = version.split('#')
                text_bug = list_ver[0]
                list_ver2 = text_bug.split('_')
                version_text = list_ver[1]+'_'+list_ver2[1]
                text.append(version_text)

            x.append(i)
            y.append(round(float(row['value']),2))
            i = i + 1

        if self.delta:
            print 'plot delta'
            for i in range(0,len(y)):
                #print 'i-1: %d i: %d' %(i,i+1)
                #time.sleep(1)
                if (i+1) == len(y):
                    valor = y[i] - y[i] 
                else:
                    valor = y[i] - y[i+1]
                #print'len %d  valor: %f' % (len(y),valor)
                
                y_delta.append(round(float(valor),2))

            y=y_delta
        


        #fig = plt.figure()
        #ax1 = fig.add_subplot(1,1,1)
        #ax1.plot(y,x)
        #ax1.set_title(title)
        #ax1.set_xlabel(xlabel)
        #ax1.set_ylabel(ylabel)
        plt.xticks(x, text,rotation=90 )

        plt.plot(x,y,'o-')

        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.grid(True)



        
        plt.savefig(nomeImg)
        plt.show()
        #plt.cla()
        #plt.clf()
        

    def plotValuesLines(self):

        if self.showBugs:
            #Retorna todas as lines
            obj = self.engine.execute("select s.version as version,pm.value as value from project_measures pm \
                        join snapshots s on (s.id = pm.snapshot_id)\
                        join metrics m on (m.id = pm.metric_id)\
                        join projects p on (p.id = s.project_id)\
                        where pm.value is not null and pm.value <> 0\
                        and m.id in (1)\
                        and length(s.version) > 0\
                        and s.version like '%%bug%%'"+\
                        "and p.name = '"+ self.nameProject +\
                        "' order by s.id desc")
        else:
            #Retorna todas as lines
            obj = self.engine.execute("select s.version as version,pm.value as value from project_measures pm \
                        join snapshots s on (s.id = pm.snapshot_id)\
                        join metrics m on (m.id = pm.metric_id)\
                        join projects p on (p.id = s.project_id)\
                        where pm.value is not null and pm.value <> 0\
                        and m.id in (1)\
                        and length(s.version) > 0\
                        and p.name = '"+ self.nameProject +\
                        "' order by s.id desc")

        
        fetch = obj.fetchall()
        self.plotGraph(fetch,'Quantidade Linhas',u'Versao','qtd Linhas','qtd_linhas.png')



    def plotValuesncloc(self):
        #Retorna todas as ncloc
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (3)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")
        
        fetch = obj.fetchall()

        self.plotGraph(fetch,'NCLOC',u'Versao','NCLOC','NCLOC.png')

    def plotValuesClasses(self):
        #Retorna todas as classes
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (5)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")
        
        fetch = obj.fetchall()

        self.plotGraph(fetch,'Quantidade Classes',u'Versao','Classes','classes.png')

    def plotValuesFiles(self):
        #Retorna todas as "files"
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (6)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")
        
        fetch = obj.fetchall()

        self.plotGraph(fetch,'Quantidade Arquivos',u'Versao','Arquivos','arquivos.png')

    def plotValuesPackages(self):
        #Retorna todas as "packages"
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (8)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")
        
        fetch = obj.fetchall()

        self.plotGraph(fetch,'Quantidade Pacotes',u'Versao','Pacotes','pacote.png')

    def plotValuesFunctions(self):
        #Retorna todas as "functions"
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (9)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")
        
        fetch = obj.fetchall()

        self.plotGraph(fetch,'Quantidade Funcao',u'Versao','Funcao','funcao.png')

    def plotValuesComplexity(self):
        #Retorna todas as "complexity"
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (20)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")
        
        fetch = obj.fetchall()

        self.plotGraph(fetch,'Complexidade',u'Versao','Complexidade','complexidade.png')

    def plotValuesRfc(self):
        #Retorna todas as "rfc"
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (116)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")
        
        fetch = obj.fetchall()

        self.plotGraph(fetch,'Rfc',u'Versao','rfc','rfc.png')

    def plotValuesLcom4(self):
        #Retorna todas as lcom4
        obj = self.engine.execute("select s.version as version,pm.value from project_measures pm\
                    join snapshots s on (s.id = pm.snapshot_id)\
                    join metrics m on (m.id = pm.metric_id)\
                    join projects p on (p.id = s.project_id)\
                    where pm.value is not null and pm.value <> 0\
                    and m.id in (118)\
                    and length(s.version) > 0\
                    and p.name = '"+ self.nameProject +\
                    "' order by s.id desc")

        fetch = obj.fetchall()

        self.plotGraph(fetch,'Lcom4',u'Versao','lcom4','lcom4.png')

