import subprocess,os
from git import *
import time
import re

def criarArquivoSonar(config_dict):
    try:
        os.remove('sonar-project.properties')
    except:
        pass

    h = open('sonar-project.properties','w')

    h.write('# Required metadata\n')
    try:
        h.write('sonar.projectKey='+config_dict['projectKey']+'\n')
    except:
        h.write('sonar.projectKey=eclipse-jdt-core\n')
    try:
        h.write("sonar.projectName="+config_dict['projectName']+"\n\n")
    except:
        h.write("sonar.projectName=This is the core part of Eclipse's Java development tools\n\n")

    try:
        h.write("sonar.projectVersion="+config_dict['projectVersion']+"\n\n")
    except:
        h.write('sonar.projectVersion=1.0\n\n')

    h.write('# Comma-separated paths to directories with sources (required)\n')

    try:
        h.write("sonar.sources="+config_dict['sources']+"\n\n")
    except:
        h.write('sonar.sources=src\n\n')

    h.write('# Language\n')
    h.write('sonar.language=java\n\n')

    h.write('# Enconding of the source files\n')
    h.write('sonar.sourceEnconding=UTF-8\n\n')


    try:
        h.write("sonar.my.property="+config_dict['my.property']+"\n\n")
    except:
        pass

    try:
        h.write("sonar.my.property="+config_dict['my.property']+"\n\n")
    except:
        pass

    try:
        h.write("sonar.binaries="+config_dict['binaries']+"\n")
    except:
        pass
        #h.write("sonar.binaries=output/classes\n")

    h.close()

def analisarRepositorio(repositorio,configAnalises):
    pastaAutal = os.getcwd()
    os.chdir(repositorio)
    p = re.compile('bug*', re.IGNORECASE)
    #inicializa o repositorio
    repo = Repo(repositorio)
    #invete a ordem dos commits
    if 'commit_reverse' in configAnalises:
        list_commit = reversed(tuple(repo.iter_commits()))
    else:
        list_commit = repo.iter_commits()
    #pega a referencia do Objeto GIT
    repo_git = repo.git
    
    count = 0
    countAux = 0
    achou = False
    qtd_analises = 1000
    qtd_analisesAux = 0
    retorno = 0
    ok = 0
    """
    for i in list_reverter:
        c=repo.commit(i)
        print time.asctime(time.gmtime(c.committed_date))
        
        if len(re.findall(p,c.message)) > 0:
            print c.message
            print time.asctime(time.gmtime(c.committed_date))
            print re.findall(p,c.message)
            print '-----------------'
        """
    for i in list_commit:
        """
        if qtd_analises > qtd_analisesAux:
        """
        if count == 0: 
            time.sleep(1)
            print i
            #Faz checkout para o branch desejado
            repo_git.clean('-df')
            repo_git.stash()
            repo_git.stash('clear')
            if configAnalises['projectname'] == 'tomcat':
                print 'Excluir Arquivos tomcat\n\n'
                subprocess.call(["sudo","rm","-Rf","/home/gabriel/topicos_especiais/tomcat/output"])
            c=repo.commit(i)
            repo_git.checkout('-f',i)
            try:
                b = open('build.properties.default','r++')
                l = b.readlines()
                l = [linha.replace('compile.target=1.7','compile.target=1.6') for linha in l]
                l = [linha.replace('compile.source=1.7','compile.source=1.6') for linha in l]
                l = [linha.replace('base.path=/usr/share/java','base.path=/home/gabriel/topicos_especiais/tomcat_download') for linha in l]
                #if configAnalises['projectname'] == 'tomcat':
                #    l.append('\nproxy.use=on\nproxy.host=proxy.domain\nproxy.port=8080\nproxy.user=username\nproxy.password=password\n')
                b.writelines(l)
                b.close()
                time.sleep(4)
            except:
                print 'Erro build.properties\n\n'
                pass

            if 'comandant' in configAnalises:
                try:
                    print 'Comando antes de rodar ant\n\n'
                    retorno=subprocess.call(["sudo","/home/gabriel/topicos_especiais/apache-ant-1.9.2/bin/ant",configAnalises['comandant']])
                except:
                    print 'Erro Comando antes ant\n\n'
                    retorno = 2

            try:
                if retorno != 2:
                    print 'Rodou ANT!'
                    if 'comandaddant' in configAnalises:
                        print 'Comando adicional ao rodar ant\n\n'
                        retorno=subprocess.call(["sudo","/home/gabriel/topicos_especiais/apache-ant-1.9.2/bin/ant","-autoproxy",configAnalises['comandaddant']])
                    else:
                        print 'Comando rodar ant\n\n'
                        retorno=subprocess.call(["sudo","/home/gabriel/topicos_especiais/apache-ant-1.9.2/bin/ant","-autoproxy"])
                    
                    if retorno == 0:
                            ok = 1;
                    if configAnalises['projectname'] == 'tomcat' and retorno != 0 and ok == 0:
                        count = 1;
                        continue

                    os.chdir(pastaAutal)
                    h1 = open('status'+configAnalises['projectname']+'.txt','a++')
                    h1.write('commit:'+str(i)+'\n\n')
                    h1.write('Status:'+str(retorno)+'\n\n')
                    h1.close()
                    os.chdir(repositorio)
            except:
                print 'Erro Rodou ANT!\n\n'
                retorno = 1



            try:
                if len(re.findall(p,c.message)) <= 0:
                    s = time.asctime(time.gmtime(c.committed_date))+'_'
                else:
                    s = time.asctime(time.gmtime(c.committed_date))+'_bug'

                s = s.replace(' ','')

                s =  s + '#' + str(i)

                s = s.replace(' ','')

                print '\n\n' + s + '\n\n'

                if retorno == 0:
                    config_dict = {
                        'projectKey' : configAnalises['projectname'],
                        'projectName' : configAnalises['projectname'],
                        'projectVersion' : s ,#'1.'+ '0'+str(qtd_analisesAux)if qtd_analisesAux < 10 else str(qtd_analisesAux),
                        'sources': configAnalises['source'],
                        #'my.property':'',
                        'binaries':configAnalises['binaries'],

                    }
                else:
                    config_dict = {
                        'projectKey' : configAnalises['projectname'],
                        'projectName' : configAnalises['projectname'],
                        'projectVersion' : s ,#'1.'+ '0'+str(qtd_analisesAux)if qtd_analisesAux < 10 else str(qtd_analisesAux),
                        'sources': configAnalises['source'],
                    }

                criarArquivoSonar(config_dict)
                #retorno = subprocess.call(["sudo","/home/gabriel/sonar/sonar-runner-2.3/bin/sonar-runner","-X"])
                print 'Rodar Sonar Runner\n\n'
                retorno = subprocess.call(["sudo","/home/gabriel/sonar/sonar-runner-2.3/bin/sonar-runner"])
                os.chdir(pastaAutal)
                h1 = open('versao'+configAnalises['projectname']+'.txt','a++')
                h1.write('versao_commit:'+str(i)+'\n')
                h1.write('status:'+str(retorno)+'\n\n')
                h1.close()
                os.chdir(repositorio)
                achou = True
                qtd_analisesAux = qtd_analisesAux + 1
                count = count + 1     
                """
                if (retorno == 0 or achou == True):
                    config_dict = {
                        'projectKey' : 'tomcat',
                        'projectName' : 'tomcat',
                        'projectVersion' : time.asctime(time.gmtime(c.committed_date)) + '_' if len(re.findall(p,c.message)) < 0 else '_bug' ,#'1.'+ '0'+str(qtd_analisesAux)if qtd_analisesAux < 10 else str(qtd_analisesAux),
                        'sources': 'java',
                        #'my.property':'',
                        'binaries':'output/classes',

                    }
                    criarArquivoSonar(config_dict)
                    retorno = subprocess.call(["sudo","/home/gabriel/sonar/sonar-runner-2.3/bin/sonar-runner","-X"])
                    h1 = open('versao.txt','a++')
                    h1.write('versao_commit:'+str(i)+'\n')
                    h1.write('status:'+str(retorno)+'\n\n')
                    h1.close()
                    achou = True
                    qtd_analisesAux = qtd_analisesAux + 1     
                else:
                    count = 5
                """
            except:
                print "ERRO rodar sonar"
                os.chdir(pastaAutal)
                h = open('erro'+configAnalises['projectname']+'.txt','a++')  
                h.write('Erro ant commit:'+str(i)+'\n\n')
                h.close()
                os.chdir(repositorio)

        else:
            print 'parou %d' % count
            if count > 5:
                count = 0
            else:
                count = count + 1
        """
        else:
            if countAux != count:
                print 'mudou versao count=' + str(countAux)
                countAux = countAux + 1
            else:
                print 'Fim versao'
                count = 0
                countAux = 0
        """

#Parou tomcat Aqui 296aca495e9616d93567f2d2217b4cf9fd22c59e
#analisarRepositorio('/home/gabriel/topicos_especiais/tomcat')
#print 'Fim! 1000'






