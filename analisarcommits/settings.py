# -*- coding-utf-8 -*-
# SQLAlchemy settings for git_data_mining

from sqlalchemy.ext.declarative import declarative_base

#Habilitar quando for teste
Teste = False

DATABASES = {
    'default':{
        'ENGINE':'postgresql+psycopg2',
        'NAME': 'gitanalyse',
        'HOST': '127.0.0.1',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'PORT': 5432,
        },
    'teste':{
        'ENGINE':'sqlite',
        'NAME': ':memory:',
        'HOST': '',
        'USER': '',
        'PASSWORD': '',
        'PORT': 0,
        },
    'sonar':{
        'ENGINE':'postgresql+psycopg2',
        'NAME': 'sonar',
        'HOST': '127.0.0.1',
        'USER': 'sonar',
        'PASSWORD': 'sonar',
        'PORT': 5432,
        },
    'sonar3':{
        'ENGINE':'postgresql+psycopg2',
        'NAME': 'sonar3',
        'HOST': '127.0.0.1',
        'USER': 'sonar',
        'PASSWORD': 'sonar',
        'PORT': 5432,
        },
    'commit':{
        'ENGINE':'postgresql+psycopg2',
        'NAME': 'commit',
        'HOST': '127.0.0.1',
        'USER': 'commit',
        'PASSWORD': 'commit',
        'PORT': 5432,
        },

}


BaseCommit = declarative_base()

