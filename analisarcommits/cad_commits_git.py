from git import *
import os
import re
from datetime import datetime as d
from models import *
from sqlalchemy.orm import sessionmaker
from xml.dom.minidom import parse, parseString


def CadCommitGit(repositorio,engine,session,nomeProjeto):
    pastaAutal = os.getcwd()
    os.chdir(repositorio)

    p = re.compile('bug*', re.IGNORECASE)

    repo = Repo(repositorio)

    repo_git = repo.git

    list_commit = repo.iter_commits()

    for i in list_commit:
        major = ''
        minor = ''
        build = ''
        patch = ''
        suffix = ''

        repo_git.clean('-df')
        repo_git.stash()
        repo_git.stash('clear')
        repo_git.checkout('-f',i)

        c = repo.commit(i)

        descricao = c.message
        commit_text = str(i)
        date_commit = d.fromtimestamp(c.committed_date)

        if len(re.findall(p,c.message)) <= 0:
            bug = False
        else:
            bug = True

        try:
            if nomeProjeto == 'jmeter' or nomeProjeto == 'poi':
                dom2 = parse('build.xml')
            else:
                f = open('build.properties.default','r')
                list_lines = f.readlines()

        
            if nomeProjeto == 'jmeter':
                for s in dom2.getElementsByTagName('property'):
                    if s.getAttribute('name') == 'jmeter.version' and s.getAttribute('value') != 'r${svn.revision}' and s.getAttribute('value') != 'r${svnCheck.revision}':
                        version = s.getAttribute('value')
            elif nomeProjeto == 'poi':
                for s in dom2.getElementsByTagName('property'):
                    if s.getAttribute('name') == 'version.id':
                        version = s.getAttribute('value')
            else:
                for l in list_lines:
                    a = l.split('=')
                    if a[0] == 'version':
                        version = a[1]
                    else:
                        if a[0] == 'version.major':
                            major = a[1].strip('\n')
                        if a[0] == 'version.minor':
                            minor = a[1].strip('\n')
                        if a[0] == 'version.build':
                            build = a[1].strip('\n')
                        if a[0] == 'version.patch':
                            patch = a[1].strip('\n')
                        if a[0] == 'version.suffix':
                            suffix = a[1].strip('\n')
                        if len(major) > 0 and len(minor) > 0 and len(build) > 0 and len(patch) > 0:
                            version = major+'.'+minor+'.'+build+'.'+patch+'.'+suffix

                if len(version) <= 0:
                    dom2 = parse('build.xml')
                    for s in dom2.getElementsByTagName('property'):
                        if s.getAttribute('name') == 'version':
                            version = s.getAttribute('value')
                        else:
                            if s.getAttribute('name') == 'version.major':
                                major = s.getAttribute('value')
                            if s.getAttribute('name') == 'version.minor':
                                minor = s.getAttribute('value')
                            if s.getAttribute('name') == 'version.build':
                                build = s.getAttribute('value')
                            if s.getAttribute('name') == 'version.patch':
                                patch = s.getAttribute('value')
                            if len(major) > 0 and len(minor) > 0 and len(build) > 0 and len(patch) > 0:
                                version = major+'.'+minor+'.'+build+'.'+patch
                        
                            version = s.getAttribute('value')

            print nomeProjeto+'='+version+'\n'+'Commit = '+commit_text

            commits_bd = Commits(commit_text,date_commit,bug,descricao,version,nomeProjeto)

            session.add(commits_bd)
        except:
            pass

    session.commit()
    os.chdir(pastaAutal)

        

