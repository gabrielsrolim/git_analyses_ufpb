# -*- coding: utf-8 -*-

from connect import connect,syncdb
from cad_commits_git import CadCommitGit
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--syncdb",action = 'store_true',help="Cria todas as tabelas que ainda faltam no banco de dados.")
    parser.add_argument("-p","--path",help="Caminho do repositorio a ser analisado")
    parser.add_argument("-n","--nameproject",help="Nome do Projeto: tomcat,jmeter,poi Default:tomcat")
    args = parser.parse_args()

    if args.nameproject != 'tomcat' and args.nameproject != 'jmeter' and args.nameproject != 'poi':
        print 'Projeto desconhecido.'
    
    engine,session = connect('commit')
    
    if args.syncdb:    
        syncdb(engine)
    else:
        print 'Do nothing!'
    print  session
    CadCommitGit(args.path,engine,session,args.nameproject)
    


