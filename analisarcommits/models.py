from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from settings import BaseCommit


class Commits(BaseCommit):
    __tablename__= 'commits'

    id = Column('id_commit',BigInteger, Sequence('id_commit_seq'),primary_key=True)
    commit = Column(String(150))
    data_commit = Column(DateTime)
    eh_bug = Column(Boolean)
    descricao = Column(Text)
    versao = Column(Text)
    nome_projeto = Column(Text)

    def __init__(self,commit,data_commit,eh_bug,descricao,versao,nome_projeto):
        self.commit = commit
        self.data_commit = data_commit
        self.eh_bug = eh_bug
        self.descricao = descricao
        self.versao = versao
        self.nome_projeto = nome_projeto

    def __repr__(self):
        return "<Commit('%s','%s', '%s', '%s')>" % (self.commit, self.data_commit, self.eh_bug, self.descricao)