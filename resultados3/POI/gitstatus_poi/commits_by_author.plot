set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "Nick Burch" w lines, 'commits_by_author.dat' using 1:3 title "Yegor Kozlov" w lines, 'commits_by_author.dat' using 1:4 title "Josh Micich" w lines, 'commits_by_author.dat' using 1:5 title "Sergey Vladimirov" w lines, 'commits_by_author.dat' using 1:6 title "Andrew C. Oliver" w lines, 'commits_by_author.dat' using 1:7 title "Glen Stampoultzis" w lines, 'commits_by_author.dat' using 1:8 title "Rainer Klute" w lines, 'commits_by_author.dat' using 1:9 title "Nicola Ken Barozzi" w lines, 'commits_by_author.dat' using 1:10 title "Avik Sengupta" w lines, 'commits_by_author.dat' using 1:11 title "Said Ryan Ackley" w lines, 'commits_by_author.dat' using 1:12 title "Dominik Stadler" w lines, 'commits_by_author.dat' using 1:13 title "Tetsuya Kitahata" w lines, 'commits_by_author.dat' using 1:14 title "Maxim Valyanskiy" w lines, 'commits_by_author.dat' using 1:15 title "Jason Height" w lines, 'commits_by_author.dat' using 1:16 title "Ugo Cei" w lines, 'commits_by_author.dat' using 1:17 title "Evgeniy Berlog" w lines, 'commits_by_author.dat' using 1:18 title "Amol S. Deshmukh" w lines, 'commits_by_author.dat' using 1:19 title "Cédric Walter" w lines, 'commits_by_author.dat' using 1:20 title "Danny Muid" w lines, 'commits_by_author.dat' using 1:21 title "Shawn Laubach" w lines
