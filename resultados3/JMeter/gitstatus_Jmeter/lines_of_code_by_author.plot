set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'lines_of_code_by_author.png'
set key left top
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Lines"
set xtics rotate
set bmargin 6
plot 'lines_of_code_by_author.dat' using 1:2 title "Sebastian Bazley" w lines, 'lines_of_code_by_author.dat' using 1:3 title "Philippe Mouawad" w lines, 'lines_of_code_by_author.dat' using 1:4 title "Peter Lin" w lines, 'lines_of_code_by_author.dat' using 1:5 title "Michael Stover" w lines, 'lines_of_code_by_author.dat' using 1:6 title "Bruno Demion" w lines, 'lines_of_code_by_author.dat' using 1:7 title "Jordi Salvat i Alabart" w lines, 'lines_of_code_by_author.dat' using 1:8 title "Jeremy Arnold" w lines, 'lines_of_code_by_author.dat' using 1:9 title "khammond" w lines, 'lines_of_code_by_author.dat' using 1:10 title "William Thad Smith" w lines, 'lines_of_code_by_author.dat' using 1:11 title "burns" w lines, 'lines_of_code_by_author.dat' using 1:12 title "kcassell" w lines, 'lines_of_code_by_author.dat' using 1:13 title "mstover" w lines, 'lines_of_code_by_author.dat' using 1:14 title "Stefano Mazzocchi" w lines, 'lines_of_code_by_author.dat' using 1:15 title "Berin Loritsch" w lines, 'lines_of_code_by_author.dat' using 1:16 title "Martijn Blankestijn" w lines, 'lines_of_code_by_author.dat' using 1:17 title "Alf Hoegemark" w lines, 'lines_of_code_by_author.dat' using 1:18 title "Scott Eade" w lines, 'lines_of_code_by_author.dat' using 1:19 title "tusharbhatia" w lines, 'lines_of_code_by_author.dat' using 1:20 title "bburns" w lines, 'lines_of_code_by_author.dat' using 1:21 title "Rainer Jung" w lines
