set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "Sebastian Bazley" w lines, 'commits_by_author.dat' using 1:3 title "Philippe Mouawad" w lines, 'commits_by_author.dat' using 1:4 title "Peter Lin" w lines, 'commits_by_author.dat' using 1:5 title "Michael Stover" w lines, 'commits_by_author.dat' using 1:6 title "Bruno Demion" w lines, 'commits_by_author.dat' using 1:7 title "Jordi Salvat i Alabart" w lines, 'commits_by_author.dat' using 1:8 title "Jeremy Arnold" w lines, 'commits_by_author.dat' using 1:9 title "khammond" w lines, 'commits_by_author.dat' using 1:10 title "William Thad Smith" w lines, 'commits_by_author.dat' using 1:11 title "burns" w lines, 'commits_by_author.dat' using 1:12 title "kcassell" w lines, 'commits_by_author.dat' using 1:13 title "mstover" w lines, 'commits_by_author.dat' using 1:14 title "Stefano Mazzocchi" w lines, 'commits_by_author.dat' using 1:15 title "Berin Loritsch" w lines, 'commits_by_author.dat' using 1:16 title "Martijn Blankestijn" w lines, 'commits_by_author.dat' using 1:17 title "Alf Hoegemark" w lines, 'commits_by_author.dat' using 1:18 title "Scott Eade" w lines, 'commits_by_author.dat' using 1:19 title "tusharbhatia" w lines, 'commits_by_author.dat' using 1:20 title "bburns" w lines, 'commits_by_author.dat' using 1:21 title "Rainer Jung" w lines
