from gitanalyse.plot import PlotAnalises
from bd.connect import connect,syncdb
import argparse

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-p","--project",help="Informa o nome do projeto para ser colocado no select de pesquisa. Defaulttomcat")
	parser.add_argument("-ph","--plothora",action = 'store_true',help="Substitui o eixo x pela hora.")
	parser.add_argument("-c","--plotcommit",action = 'store_true',help="Substitui o eixo x pelo numero do commit.")
	parser.add_argument("-bd","--banco",help="Informe a configuracao do banco: sonar ou sonar3")
	parser.add_argument("-pd","--plotdelta",action = 'store_true',help="Gera graficos com o delta das metricas.")
	parser.add_argument("-sb","--showbug",action = 'store_true',help="Exibe graficos apenas de bugs")
	args = parser.parse_args()

	print 'banco: %s' % args.banco

	if args.banco:
		engine,session = connect(args.banco)
	else:
		engine,session = connect('sonar3')

	if args.plotdelta:
		print 'Plot Delta'

	if not args.plothora and not args.plotcommit:
		args.plothora = True;

	if args.plotcommit:
		args.plothora = False;

	if args.project:
		plot = PlotAnalises(engine,args.project,args.plothora,args.plotcommit,args.plotdelta,args.showbug)
	else:
		plot = PlotAnalises(engine)

	
	plot.plotValuesLines()
	plot.plotValuesncloc()
	plot.plotValuesClasses()
	plot.plotValuesFiles()
	plot.plotValuesPackages()
	plot.plotValuesFunctions()
	plot.plotValuesComplexity()
	plot.plotValuesRfc()
	plot.plotValuesLcom4()






